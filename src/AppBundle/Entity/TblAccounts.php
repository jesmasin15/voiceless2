<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblAccounts
 *
 * @ORM\Table(name="tbl_accounts")
 * @ORM\Entity
 */
class TblAccounts
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=225, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=225, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="text", length=65535, nullable=true)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=65535, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=100, nullable=true)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=225, nullable=true)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="userID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userid;




    public function getAccountId(){
        return  $this->userid;
    }
    public function getUsername(){
        return  $this->username;
    }
    public function getPassword(){
        return  $this->password;
    }
    public function getFullname(){
        return  $this->fullname;
    }
    public function getAddress(){
        return  $this->address;
    }
    public function getContact(){
        return  $this->contact;
    }
     public function getImage(){
        return  $this->image;
    }

    public function setAccountId($id){
        $this->userid = $id;
    }
    public function setUsername($username){
        $this->username = $username;
    }
    public function setPassword($password){
        $this->password = $password;
    }
    public function setFullname($fullaname){
        $this->fullname = $fullaname;
    }
    public function setAddress($address){
        $this->address = $address;
    }
    public function setContact($contact){
        $this->contact = $contact;
    }
    public function setImage($image){
        $this->image = $image;
    }

}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblPost
 *
 * @ORM\Table(name="tbl_post")
 * @ORM\Entity
 */
class TblPost
{
    /**
     * @var string
     *
     * @ORM\Column(name="postTitle", type="string", length=225, nullable=true)
     */
    private $posttitle;

    /**
     * @var string
     *
     * @ORM\Column(name="postAuthor", type="string", length=225, nullable=true)
     */
    private $postauthor;

    /**
     * @var string
     *
     * @ORM\Column(name="postContent", type="text", length=65535, nullable=true)
     */
    private $postcontent;

    /**
     * @var integer
     *
     * @ORM\Column(name="postLikes", type="integer", nullable=true)
     */
    private $postlikes = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="postComments", type="integer", nullable=true)
     */
    private $postcomments = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="postImage", type="string", length=225, nullable=true)
     */
    private $postimage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="postDate", type="datetime", nullable=true)
     */
    private $postdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="postId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $postid;
    
 public function getPostId(){
        return  $this->postid;
    }
    public function getTitle(){
        return  $this->posttitle;
    }
    public function getAuthor(){
        return  $this->postauthor;
    }
    public function getContent(){
        return  $this->postcontent;
    }
    public function getLikes(){
        return  $this->postlikes;
    }
    public function getComments(){
        return  $this->postcomments;
    }
    public function getImage(){
        return  $this->postimage;
    }
    public function getDate(){
        return  $this->postdate;
    }





    public function setPostId($id){
        $this->postid = $id;
    }
    public function setTitle($title){
        $this->posttitle = $title;
    }
    public function setAuthor($author){
        $this->postauthor = $author;
    }
    public function setContent($content){
        $this->postcontent = $content;
    }
    public function setLikes($likes){
        $this->postlikes = $likes;
    }
    public function setComments($comments){
        $this->postcomments = $comments;
    }
    public function setImage($image){
        $this->postimage = $image;
    }
    public function setDate(\DateTime $date){
        $this->postdate = $date;
    }

}


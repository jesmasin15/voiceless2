<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblComments
 *
 * @ORM\Table(name="tbl_comments")
 * @ORM\Entity
 */
class TblComments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commPostId", type="integer", nullable=true)
     */
    private $commpostid;

    /**
     * @var string
     *
     * @ORM\Column(name="commReceiver", type="string", length=225, nullable=true)
     */
    private $commreceiver;

    /**
     * @var string
     *
     * @ORM\Column(name="commContent", type="text", length=65535, nullable=true)
     */
    private $commcontent;

    /**
     * @var string
     *
     * @ORM\Column(name="commSender", type="string", length=200, nullable=true)
     */
    private $commsender;

    /**
     * @var integer
     *
     * @ORM\Column(name="commId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commid;



public function getCommentId(){
        return  $this->commid;
    }
    public function getPostId(){
        return  $this->commpostid;
    }
    public function getReceiver(){
        return  $this->commreceiver;
    }
    public function getComment(){
        return  $this->commcontent;
    }
    public function getSender(){
        return  $this->commsender;
    }


      public function setCommentId($id){
        $this->commid = $id;
    }
    public function setPostId($id){
        $this->commpostid = $id;
    }
    public function setReceiver($receiver){
        $this->commreceiver = $receiver;
    }
    public function setComment($content){
        $this->commcontent = $content;
    }
    public function setSender($sender){
        $this->commsender = $sender;
    }


}



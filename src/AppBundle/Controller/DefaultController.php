<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\TblAccounts;  
use AppBundle\Entity\TblPost;  
use AppBundle\Entity\TblComments;  
use AppBundle\Form\LoginType;
use AppBundle\Form\PostType;
use AppBundle\Form\CommentType;
use AppBundle\Form\ProfileType;
use AppBundle\Form\EditProfileType;

class DefaultController extends Controller
{


     private $session;

    public function __construct(){
        $this->session = new Session();
    }


    /**
     * @Route("/homepage", name="homepage")
     */
    public function indexAction(Request $request)
    {
    
        if($this->session->has("username")){
                
        $em = $this->getDoctrine()->getManager();
        $prod = new TblPost();
        $form = $this->createForm(PostType::class,$prod);

        $form->handleRequest($request);

        $repo = $this->getDoctrine()->getManager()->getRepository(TblPost::class);
        $array = array();   
        $product =$repo->findAll();
        $x=0;
        foreach($product as $key=>$value){
            $array[$x]["Id"] = $value->getPostId();
            $array[$x]["Title"] = $value->getTitle();
            $array[$x]["Author"] = $value->getAuthor();
            $array[$x]["Content"] = $value->getContent();
            $array[$x]["Likes"] = $value->getLikes();
            $array[$x]["Comments"] = $value->getComments();
            $array[$x]["Image"] = $value->getImage();
            $array[$x]["Date"] = $value->getDate();
            $x++;
        }


        if($form->isSubmitted() && $form->isValid()){

            $time = new \DateTime('now');
            $file = $prod->getImage();          
          
            $filename =uniqid().".".$file->guessExtension();
            
            $file->move("upload/img",$filename);

            $prod->setImage($filename);
            $prod->setDate($time);
            $prod->setAuthor($this->session->get("username"));
            $em->persist($prod);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }

          return $this->render("default/index.html.twig",array('form'=>$form->createView(),"post"=>$array, 'username'=>$this->session->get("username")));
          }
          else{
             return $this->redirectToRoute('login');
          }


      
    }


    /**
     * @Route("/login", name="login")
     */
public function loginView(Request $request, Request $req)
{

     if($this->session->has("username"))
     {
                      return $this->redirectToRoute('homepage');
     }

     else{


           $em = $this->getDoctrine()->getManager();

                            $acct = new TblAccounts();

                            $form = $this->createForm(LoginType::class,$acct);

                                $form2 = $this->createForm(ProfileType::class,$acct);

                                $form2->handleRequest($req);

                            $form->handleRequest($request);

                            $repo = $this->getDoctrine()->getManager()->getRepository(TblAccounts::class);

 if($form2->isSubmitted() && $form2->isValid()){

            $file = $acct->getImage();          
            $filename =uniqid().".".$file->guessExtension();
            
            $file->move("upload/img",$filename);

            $acct->setImage($filename);

                    $em->persist($acct);
                    $em->flush();

            

           return $this->redirectToRoute('login');
       }                        

         if($form->isSubmitted() && $form->isValid()){

                $data = $form->getData();

                $ausername = $data->getUsername();

                $apassword = $data->getPassword();

                $accounts =$repo->findOneBy(['username'=>$ausername, 'password'=>$apassword]);

                

                        if(!empty($accounts)){
                        
                    
                        $acctUsername = $accounts->getUsername();
                        $acctPassword = $accounts->getPassword();            
                         $this->session->set("username",$acctUsername);                 
                        
                    

                           
                            $username = $this->session->get("username");                
                            
                                if(!empty($acctUsername) && !empty($acctPassword)){
                                    

                                  
                                return $this->redirectToRoute('homepage');

                                }

                                else{
                                    echo '<script>alert("Incorrect Username or Password!");</script>';
                                    return $this->redirectToRoute('login');

                                }   
                       }

                        else{
                                        echo '<script>alert("Please input your username and password!");</script>';
                                        return $this->redirectToRoute('login');                        
                        }
       }
   }
   return $this->render("default/login.html.twig",array('form'=>$form->createView(),'register'=>$form2->createView()));
}

  
  /**
     * @Route("/logout", name="logout")
     */

    public function logout(){

        $this->session->remove("username");

         return $this->redirectToRoute('login');                        
    }

    /**
     * @Route("/like/{id}/{status}", name="like")
     */

    public function like($id,$status){
         $count = 0;
           $em = $this->getDoctrine()->getManager();
            $post = $em->getRepository(TblPost::class)->find($id);
                $likes = $post->getLikes(); 

                 if($this->session->has("likes")[$count]){
                 
                
                        $total_likes = $likes-1;

                         $post->setLikes($total_likes);
                        $this->session->remove("likes")[$count];

                            $em->flush();
                         return $this->redirectToRoute('homepage');                        
                        }

                        else{
            
            
                                   $total_likes = $likes+1;
                                   
                                     $post->setLikes($total_likes);
                                       $em->flush();
                                       $post_status[$count] = $status; 
                                        $this->session->set("status",$post_status);
                                        $count = $count + 1;
                                return $this->redirectToRoute('post',array('id' => $id));                       
                            }
         }


        /**
         * @Route("/post/{id}", name="post")
         */
         public function viewPost($id,Request $request){
             $em2 = $this->getDoctrine()->getManager();
                $post = $em2->getRepository(TblPost::class)->find($id);

                  
                      
                         $array2 = array();  
                            $array = array(); 


           if($this->session->has("username"))
             {
              
              
            $array["Id"] = $id;
            $array["Title"] = $post->getTitle();
            $array["Author"] = $post->getAuthor();
            $array["Content"] = $post->getContent();
            $array["Likes"] = $post->getLikes();
            $array["Comments"] = $post->getComments();
            $array["Image"] = $post->getImage();
           
//FOR COMMENTS IN POST
 

                          $x=0;
        $repo = $this->getDoctrine()->getManager()->getRepository(TblComments::class);
        $comm = $repo->findAll();
        foreach($comm as $key=>$value){
            if($value->getPostId() == $id){
            $array2[$x]["Id"] = $value->getCommentId();
            $array2[$x]["PostId"] = $value->getPostId();
            $array2[$x]["Receiver"] = $value->getReceiver();
            $array2[$x]["Content"] = $value->getComment();
            $array2[$x]["Sender"] = $value->getSender();
           }
           else{
             $array2[$x]["Id"] = '';
            $array2[$x]["PostId"] = '';
            $array2[$x]["Receiver"] = '';
            $array2[$x]["Content"] = '';
            $array2[$x]["Sender"] = '';
           }
            $x++;
        }    
      
 

                 $em = $this->getDoctrine()->getManager();
                    $prod = new TblComments();
                    $form = $this->createForm(CommentType::class,$prod);
                     $form->handleRequest($request);

                  if($form->isSubmitted() && $form->isValid()){
                    $count = $post->getComments();
                    $count = $count + 1;   
                    $prod->setPostId($id);
                    $prod->setReceiver($this->session->get("username"));
                    $prod->setSender($array["Author"]); 
                    $post->setComments($count);
                    $em2->flush();
                    $em->persist($prod);
                    $em->flush();

                     return $this->redirectToRoute('post',array('id' => $id)); 
                  }

             return $this->render("default/post.html.twig",array('form'=>$form->createView(),"post"=>$array,'comm'=>$array2));
            }









            else{
                 return $this->redirectToRoute('login');
            }
    }


     /**
         * @Route("/profile", name="profile")
         */

     public function viewProfile(Request $req){

                         $array2 = array();  
                            $array = array(); 


            $em2 = $this->getDoctrine()->getManager();
                $acct = $em2->getRepository(TblAccounts::class)->findOneBy(['username' => $this->session->get("username")]);

       
        
       

        $form = $this->createForm(EditProfileType::class,$acct);

        $form->handleRequest($req);


                  if($form->isSubmitted() && $form->isValid()){
                      $em3 = $this->getDoctrine()->getManager();
                        $post = $em3->getRepository(TblPost::class)->findOneBy(['postauthor' => $this->session->get("username")]);
                          $data = $form->getData();
                             $ausername = $data->getUsername();
                                $post->setAuthor($ausername);

                                    $file = $acct->getImage();          
                                        $filename =uniqid().".".$file->guessExtension();
            
                                            $file->move("upload/img",$filename);

                                                    $acct->setImage($filename);
                                  $em3->flush();
                                      $em2->flush();


                        echo '<script>alert("Profile Changed. Please Log in Again.")</script>';
                         return $this->redirectToRoute('logout');
                    }
                  
                      
           if($this->session->has("username"))
             {
              
              
              $array["Id"] = $acct->getAccountId();
            $array["Username"] = $acct->getUsername();
            $array["Password"] = $acct->getPassword();
            $array["Name"] = $acct->getFullname();
            $array["Address"] = $acct->getAddress();           
            $array["Contact"] = $acct->getContact();
            $array["Image"] = $acct->getImage();

 $repo = $this->getDoctrine()->getManager()->getRepository(TblPost::class);
            
        $product =$repo->findBy(['postauthor'=> $this->session->get("username")]);
        $x=0;

        if($product != null){
        foreach($product as $key=>$value){
            $array2[$x]["Id"] = $value->getPostId();
            $array2[$x]["Title"] = $value->getTitle();
            $array2[$x]["Author"] = $value->getAuthor();
            $array2[$x]["Content"] = $value->getContent();
            $array2[$x]["Likes"] = $value->getLikes();
            $array2[$x]["Comments"] = $value->getComments();
            $array2[$x]["Image"] = $value->getImage();
            $array2[$x]["Date"] = $value->getDate();
            $x++;

            
            }

        }
         else{
                $array2= null;
            }
        }
        else{
            return $this->redirectToRoute('login');
        }

          return $this->render("default/profile.html.twig", array('acct'=>$array,'post'=>$array2,'register'=>$form->createView()));
     
 }
  

      /**
     * @Route("/dataDelete/{id}", name="deleteData")
     */
    function deleteAction($id){
    

        $em = $this->getDoctrine()->getManager();
        $prod = $em->getRepository(TblPost::class)->findOneBypostid($id);

       
if($prod != null){
        $em2 = $this->getDoctrine()->getManager();
        $prod2 = $em->getRepository(TblComments::class)->findOneBy(['commpostid'=>$id]);
        $em2->remove($prod2);
        $em2->flush();
         $em->remove($prod);
        $em->flush();
                   return $this->redirectToRoute('profile');
    }
    else{
               return $this->redirectToRoute('profile');
    }

         
    }
  
}



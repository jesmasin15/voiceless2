<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
* 
*/
class PostType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $fbi, array $options){

			$fbi->add('Title',TextType::class,array(
   					 'attr' => array('class'=>'form-control','id'=>'title', 'name'=>'title', 'style' => 'width: 500px', 'placeholder' => 'Enter Title Here...')));
			$fbi->add('Content',TextAreaType::class,array(
   					 'attr' => array('class'=>'form-control','id'=>'content', 'name'=>'content', 'style' => 'width: 500px', 'placeholder' => 'Enter Content Here...', 'rows' => '6')));			
			$fbi->add('Image',FileType::class,array(
   					 'attr' => array('id'=>'content', 'name'=>'content')));			
			$fbi->add('Save',SubmitType::class,array(
   					 'attr' => array('class'=>'btn btn-primary','id'=>'content', 'name'=>'content', 'style' => 'width: 100px; margin-top: 10px;')));			
	}
}


?>
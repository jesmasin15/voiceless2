<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

/**
* 
*/
class LoginType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $fbi, array $options){

			$fbi->add('username',TextType::class,array(
   					 'attr' => array('class'=>'rotate','id'=>'boxy-input', 'name'=>'username', 'step'=>'0')));

			$fbi->add('password',PasswordType::class, array(
				'attr'=> array('class'=>'rotate','id'=>'boxy-password','name' => 'password', 'placeholder' => 'Enter your password here...', 'step'=> '2' )));			
			$fbi->add('Login',SubmitType::class);

		}
}


?>
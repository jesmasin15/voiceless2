<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

/**
* 
*/
class EditProfileType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $fbi, array $options){


		
		$fbi->add('username',TextType::class,array(
   					 'attr' => array('class'=>'form-first-name form-control','id'=>'form-first-name', 'name'=>'form-first-name', 'placeholder' => 'Username')));
			$fbi->add('password',PasswordType::class,array(
   					 'attr' => array('class'=>'form-first-name form-control','id'=>'form-first-name', 'name'=>'form-first-name', 'placeholder' => 'New Password', 'required'=>'')));
			$fbi->add('fullname',TextType::class,array(
   					 'attr' => array('class'=>'form-first-name form-control','id'=>'form-first-name', 'name'=>'form-first-name', 'placeholder' => 'Enter your Full Name...')));
			$fbi->add('address',TextareaType::class,array(
   					 'attr' => array('class'=>'form-about-yourself form-control','id'=>'form-about-yourself', 'name'=>'form-about-yourself', 'placeholder' => 'Enter your Address')));
			$fbi->add('contact',TextType::class,array(
   					 'attr' => array('class'=>'form-first-name form-control','id'=>'form-first-name', 'name'=>'form-first-name', 'placeholder' => 'Contact')));
			$fbi->add('image',FileType::class,array(
   					 'attr' => array('class'=>'btn'),'data_class' => null));
			$fbi->add('save',SubmitType::class,array(
   					 'attr' => array('class'=>'btn')));

		}
}


?>